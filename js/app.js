/**
 * Created by amanry on 6/2/14.
 */

var canvas,stage, ball, learnToCount,
    min=1,max=10,total,
    counter=0,x=50,y=200,radius=40,userCount=1,
    audioPlaying = false, fontSize=40, numCols=5,numRows=1, rowCounter= 1,colCounter= 1,
    done=false, greatJobPlayed=false, retina=false, smileyScale=1;

function init() {
    stage = new createjs.Stage("myCanvas");

    canvas = document.getElementById("myCanvas");
    optimizeScreen();
    //window.addEventListener('resize', reloadCanvas, false);
    drawCircles();
    //create LearnToCount app
    learnToCount = new ns.LearnToCount();
}

function drawCircles() {
    var remainder = total%numCols;
    console.log("total:" + total + " remainder:" + remainder);

    while (counter<total){
        //loop through each row
        while (rowCounter<=numRows) {
            //if we are not on the last row OR total==numCols
            if (rowCounter<numRows || total==numCols) {
                //loop through each col
                while (colCounter <= numCols) {
                    drawBall(x, y, radius);
                    colCounter++;
                    counter++;
                }
                //reset colCounter to 1
                colCounter=1;
            }
            //if we are on the last row, then find how many circles to draw
            else if (rowCounter==numRows) {
                var numCirclesLastRow;
                if (remainder==0) numCirclesLastRow=numCols;
                else numCirclesLastRow = remainder;
                //loop through each col
                while (colCounter <= numCirclesLastRow) {
                    drawBall(x, y, radius);
                    colCounter++;
                    counter++;
                }
                //reset colCounter to 1
                colCounter=1;

            }

            rowCounter++;
        }
        //reset rowCounter back to 1
        rowCounter=1;
        counter++;
    }
}

/*
This function will optimize the real estate of the screen so that the circles are as large as possible.
The following guidelines are implemented here:
    - If we are on a large screen (desktop or tablet), then display 5 circles on each row
    - If on a phone and screen orientation is portrait, then display only 3 circles per row
    - If on a phone and screen orientation is landscape, then display 5 circles on each row
 */
function optimizeScreen(){
    console.log("in optimizeScreen");
    if (window.devicPixelRatio>1) retina=true;
    if (retina) {
        canvas.width=window.screen.availWidth*window.devicePixelRatio;
        canvas.height=window.screen.availHeight*window.devicePixelRatio;
        if (canvas.width/window.devicePixelRatio <= 800 && canvas.width < canvas.height) {
            numCols = 2;
            max = 5; //only go up to 5 on a phone
            smileyScale = .25;  //make smailey face one quarter smaller for a phone
        }
    } else {
        canvas.width = window.innerWidth;
        canvas.height = window.innerHeight;
        if (canvas.width <= 800  && canvas.height <= 1200 && canvas.width < canvas.height) {
            numCols = 2;
            max = 5; //only go up to 5 on a phone
        }
    }

    console.log("pixelDepth is: " + window.screen.pixelDepth);


    total = getRandomInt(min,max);

    numRows = Math.ceil(total/numCols);
    radius = Math.round(canvas.width/(numCols*2+numCols+1));
    fontSize = radius;
    console.log("width:" +canvas.width+ " height:" + canvas.height
        + " cols:" + numCols + " rows:" + numRows + " radius:" + radius);
    x=2*radius;
    y=2*radius;
}

function reloadCanvas() {
    canvas.width = window.innerWidth;
    canvas.height = window.innerHeight;
    location.reload();
}

/**
 * Returns a random integer between min (inclusive) and max (inclusive)
 * Using Math.round() will give you a non-uniform distribution!
 */
function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

function drawBall(x,y,radius) {
    ball = new createjs.Shape();
    ball.name = counter;
    ball.graphics.beginFill("#000000").drawCircle(0,0,radius);
    //set x and y values
    ball.x = x + radius*(colCounter-1)*3;
    ball.y = y + radius*(rowCounter-1)*3;
    ball.addEventListener("click", this.handleClick);
    stage.addChild(ball);
    stage.update();

}


function handleClick(event) {
    var target = event.target;

    //if user has clicked on the next circle to count && there is no current audio playing
    if(target.name == userCount-1 && !audioPlaying){
        //play audio & display number
        learnToCount.playAudio(userCount);
        drawText((userCount==10)?target.x-radius/2:target.x-radius/3,target.y-radius/2);
        if (userCount==total) {
            done=true;
            //return.  no need to increment userCount
            return;
        }
        userCount++;
    }


}

function drawText(textX,textY){
    var text = new createjs.Text(userCount, radius+"px Arial", "#ff7700");
    text.x = textX;
    text.y = textY;
    stage.addChild(text);
    stage.update();
}


this.ns = this.ns || {};
(function() {
    function LearnToCount() {
        this.init();
    }


    LearnToCount.prototype = {

        //init function to preload audio
        init: function() {

            queue = new createjs.LoadQueue(false);
            if (!createjs.Sound.initializeDefaultPlugins()) {return;}

            var audioPath = "assets/";
            var manifest = [
                {id:"circles", src:"countcircles.ogg"},
                {id:"1", src:"1.ogg"},
                {id:"2", src:"2.ogg"},
                {id:"3", src:"3.ogg"},
                {id:"4", src:"4.ogg"},
                {id:"5", src:"5.ogg"},
                {id:"6", src:"6.ogg"},
                {id:"7", src:"7.ogg"},
                {id:"8", src:"8.ogg"},
                {id:"9", src:"9.ogg"},
                {id:"10", src:"10.ogg"},
                {id:"greatjob", src:"greatjob.ogg"}
            //audio files to load
            ];

            createjs.Sound.alternateExtensions = ["mp3"];
            var loadProxy = createjs.proxy(this.handleLoad, this);
            createjs.Sound.addEventListener("fileload", loadProxy);
            createjs.Sound.registerManifest(manifest, audioPath);


        },

        //after load is completed
        handleLoad: function(event) {
            var instance = createjs.Sound.play("circles");  // play using id.  Could also use full sourcepath or event.src.
            audioPlaying = true;
            instance.addEventListener("complete", createjs.proxy(this.handleComplete, this));
        },

        //after audio is finished
        handleComplete: function(event) {
            audioPlaying = false;
            if(userCount==total && done && !greatJobPlayed) {
                this.playAudio("greatjob");
                //ensure that we only play this once. otherwise we are in infinite loop
                greatJobPlayed=true;
                this.drawSmiley();

            }
        },

        playAudio: function(id) {
            var instance = createjs.Sound.play(id);
            audioPlaying = true;
            instance.addEventListener("complete", createjs.proxy(this.handleComplete, this));
        },

        drawSmiley: function(){

            var s = new createjs.Shape();
            var g = s.graphics;
            //Head
            g.setStrokeStyle(15, 'round', 'round');
            g.beginStroke("#000");
            g.beginFill("#F00");
            g.drawCircle(170*smileyScale, 170*smileyScale, 170*smileyScale); //55,53
            g.endFill();
            g.setStrokeStyle(1, 'round', 'round');

            s.addEventListener("click", reloadCanvas, false);

            //Right eye
            g.setStrokeStyle(5, 'round', 'round');
            g.beginStroke("#000");
            g.beginFill("#000");
            g.drawRoundRect(125*smileyScale, 64*smileyScale, 20*smileyScale, 50*smileyScale, 10*smileyScale);
            g.endFill();

            //Left eye
            g.setStrokeStyle(5, 'round', 'round');
            g.beginStroke("#000");
            g.beginFill("#000");
            g.drawRoundRect(200*smileyScale, 64*smileyScale, 20*smileyScale, 50*smileyScale, 10*smileyScale);
            g.endFill();

            //Mouth
            g.setStrokeStyle(15, 'round', 'round');
            g.beginStroke("#000");
            g.moveTo(45*smileyScale, 155*smileyScale);
            g.bezierCurveTo(83*smileyScale, 307*smileyScale, 254*smileyScale, 317*smileyScale, 296*smileyScale, 152*smileyScale);
            stage.addChild(s);
//            var s = new createjs.Shape();
//            var g = s.graphics;
//
//            //Head
//            g.setStrokeStyle(15, 'round', 'round');
//            g.beginStroke("#000");
//            g.beginFill("#F00");
//            g.drawCircle(canvas.width/2, canvas.height/2, canvas.height/2); //55,53
//            g.endFill();
//            g.setStrokeStyle(1, 'round', 'round');
//
//            s.addEventListener("click", reloadCanvas, false);
//
//            //Right eye
//            g.setStrokeStyle(5, 'round', 'round');
//            g.beginStroke("#000");
//            g.beginFill("#000");
//            g.drawRoundRect(canvas.width/2-canvas.width/8, canvas.height/4, 20, 100, 10);
//            g.endFill();
//
//            //Left eye
//            g.setStrokeStyle(5, 'round', 'round');
//            g.beginStroke("#000");
//            g.beginFill("#000");
//            g.drawRoundRect(canvas.width/2+canvas.width/8, canvas.height/4, 20, 100, 10);
//            g.endFill();
//
//            //Mouth
//            g.setStrokeStyle(15, 'round', 'round');
//            g.beginStroke("#000");
////            g.moveTo(45, 155);
////            g.bezierCurveTo(83, 307, 254, 317, 296, 152);
//            g.moveTo(canvas.width/2-canvas.width/8,canvas.height/2+canvas.height/4);
//            g.bezierCurveTo(canvas.width/2, canvas.height/2+canvas.height/3, canvas.width/2, canvas.height/2+canvas.height/3, canvas.width/2+canvas.width/8, canvas.height/2+canvas.height/4);
//            stage.addChild(s);

            stage.update();

        }



    } //end prototype

    //add app to namespace
    ns.LearnToCount = LearnToCount;
    }
());
